#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Check data set names for:
- Duplicated names
- Special characters other than digits, letters and _
- all names follow taxonID@seqID, and file names are the taxonID
"""

import os
from re import match, sub
import argparse
from SeqBuddy import SeqBuddy, find_repeats

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='check_names', description='Ensure that there are no duplicate ids, and taxon names are in place')

    parser.add_argument('dir', help='Location of all files to be checked', action='store')
    parser.add_argument('extensions', help="List each extension type to be checked", nargs="+", action='store')

    in_args = parser.parse_args()

    in_args.extensions = [sub("\.", "", ext) for ext in in_args.extensions]

    assert os.path.isdir(in_args.dir)
    in_args.dir = os.path.abspath(in_args.dir)

    root, dirs, files = next(os.walk(in_args.dir))
    seq_files = []

    for _file in files:
        if _file.split(".")[-1] in in_args.extensions:
            seq_files.append("%s/%s" % (in_args.dir, _file))

    seqbuddy_objs = []
    for _file in seq_files:
        taxonID = _file.split("/")[-1].split(".")[0]
        seqbuddy_objs.append(SeqBuddy(_file))
        seqbuddy_objs[-1].taxonID = taxonID

    total_tested = 0
    total_passed = 0
    
    for seqbuddy in seqbuddy_objs:
        all_clean = True
        print("### %s ###" % seqbuddy.taxonID)
        err_taxids = []
        err_chars = []
        seqIDs = []

        for rec in seqbuddy.records:
            spls = rec.id.split("@")
            if spls[0] != seqbuddy.taxonID:
                err_taxids.append(rec.id)

            if not match("^[a-zA-Z0-9_ΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥϒΦΧΨΩαβγδεζηθϑικλμνξοπϖρςστυφχψωϑϒϖ]+$", spls[1]):
                err_chars.append(rec.id)

            seqIDs.append(rec.id)

        unique_seqs, repeat_ids, repeat_seqs = find_repeats(seqbuddy)

        if len(err_taxids) > 0:
            all_clean = False
            print("\tThe following sequences are not prefixed with the correct taxon ID:\n\t%s\n" % err_taxids)

        if len(err_chars) > 0:
            all_clean = False
            print("\tThe following sequences contain illegal characters in their names:\n\t%s\n" % err_chars)

        if len(repeat_ids) > 0:
            all_clean = False
            repeat_ids = [_id for _id in repeat_ids]
            print("\tThe following sequences have identical ids:\n\t%s\n" % repeat_ids)

        total_tested += len(seqIDs)
        if all_clean:
            total_passed += len(seqIDs)
            print("\t%s sequences passed\n" % len(seqIDs))

        else:
            num_clean = len(seqIDs) - len(set(repeat_ids + err_chars + err_taxids))
            total_passed += num_clean
            print("\t%s of %s sequences passed\n" % (num_clean, len(seqIDs)))

    print("%s sequences tested in %s files, %s passed" % (total_tested, len(seqbuddy_objs), total_passed))
