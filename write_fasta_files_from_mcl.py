#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Read the concatenated fasta file either with or without ends cut
write individual fasta files for each cluster
"""

import sys
from Bio import SeqIO

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("usage: write_fasta_files_from_mcl.py fasta mcl_outfile minimal_taxa outDIR")
        sys.exit()

    fasta = sys.argv[1]
    mclfile = sys.argv[2]
    MIN_TAXA = int(sys.argv[3])
    outDIR = sys.argv[4] + "/"

    print("Reading mcl output file")
    clusterDICT = {}  # key is seqID, value is clusterID
    count = 0
    with open(mclfile, "rU") as infile:
        for line in infile:
            if len(line) < 3:
                continue  # ignore empty lines
            spls = line.strip().split('\t')
            taxa = []
            for seqID in spls:
                taxonID = seqID.split("@")[0]
                if taxonID not in taxa:
                    taxa.append(taxonID)
            if len(taxa) >= MIN_TAXA:
                count += 1
                clusterID = str(count)
                for seqID in spls:
                    clusterDICT[seqID] = clusterID

    print("Reading the fasta file")
    handle = open(fasta, "rU")
    for record in SeqIO.parse(handle, "fasta"):
        seqid, seq = str(record.id), str(record.seq)
        try:
            clusterID = clusterDICT[seqid]
            with open(outDIR + "cluster" + clusterID + ".fa", "a") as outfile:
                outfile.write(">" + seqid + "\n" + seq + "\n")
        except:
            pass
    handle.close()
