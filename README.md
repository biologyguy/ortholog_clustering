Citation: Yang, Y. and S.A. Smith. 2014. Orthology inference in non-model organisms using transcriptomes and low-coverage genomes: improving accuracy and matrix occupancy for phylogenomics. Molecular Biology and Evolution. doi: 10.1093/molbev/msu245

All sequence IDs are formated as taxonID@seqID. Use the special character "@" to separate taxonID and seqID. Avoid using any other special characters in the sequence names except "_". Any "-" in the sequence name will be replaced by phyutility and cause problems in later stages when matching sequence names between tree and alignment. Make sure that each taxon have exactly the same taxonID before clustering.

Note on taxon sampling: since any clustering algorithm is sensitive to structure within an ortholog group, it is best to avoid including two or more taxa that are significantly more closely-related to each other than to rest of the taxa. 

##Step 1: Processing Illumina reads
Choose a taxonID for each data set. This taxonID will be used throughout the analysis. Name each fastq file taxonID_1.fq and taxonID_2.fq.

Remove adapters and filter low quality reads. Use the scripts filter_fastq.py in the optimizing_assembler repository. The scripts also requires the sequence.py and seq_reader.py in the optimizing_assembler repository. Check and change the phred score offset in sequence.py when needed.

	python filter_fastq.py taxonID_1.fq taxonID_2.fq adapter_file num_cores

Alternatively, if sequences are single end reads:
	
	python filter_fastq.py taxonID.fq adapter_file num_cores

The output files are taxonID_1.fq.filtered and taxonID_2.fq.filtered for paired end reads, and taxonID.fq for single end reads. Use these two filtered fastq files as input for Trinity.

##Step 2: *De novo* assembly with Trinity

Check to make sure read ends are marked with "/1" and "/2" for paired-end reads.

	ulimit -s unlimited
	Trinity --seqType fq --max_memory 40G --left <absolute path to taxonID_1.fq.filtered> --right <absolute path to taxonID_2.fq.filtered> --CPU <num_cores> --output <taxonID>

The output file is taxonID.Trinity.fasta. Shorten the transcript names and remove all the spaces. BioPython, blast and a variety of other downstream analyses will not process seqIDs correctly when they contain spaces. Standerize transcript names to taxonID@seqID. Avoid any special characters in the transcript names.

	sed -e 's/>/>taxonID@/g' Trinity.fasta >taxonID.Trinity.fasta
	sed -i -e 's/ .*//g' taxonID.Trinity.fasta

##Step 3: Find open reading frames and translate using TransDecoder

A variety of translators can be used for translation. Here we use the pfam guided transdecoder.

	TransDecoder -t taxonID.Trinity.fasta --search_pfam <path to transdecoder dir>/pfam/Pfam-AB.hmm.bin --CPU 5

Annotate transcripts

	Space holder

##Step 4: Clustering

The TransDecoder be taxonID.Trinity.fasta.transdecoder.pep and taxonID.Trinity.fasta.transdecoder.cds. Shorten the names for peptides and CDS. Make sure that the peptide and cds sequences having the same name for back translation later, and there is no space or special characters in these names except "@" and "_".

	python fix_names_from_transdecoder.py <inDIR> <outDIR>

Once all data sets are read, check for duplicated names, special characters other than digits, letters and "_", and all names follow taxonID@seqID, and file names are the taxonID. It's good to check especially when some of the data sets were obtained elsewhere. Most genome annotation files contain long names, spaces and special characters.

	python check_names.py <DIR> <file_ending>

Reduce redundancy by similarity. This is optional since high similarity seqs can also be illiminated  For amino acids:

	cd-hit -i taxonID.fa -o taxonID.fa.cdhit -c 0.995 -n 5 -T <num_cores>

Or alternatively, for cds:

	cd-hit-est -i taxonID.fa.cds -o code.fa.cds.cdhitest -c 0.99 -n 10 -r 0 -T <num_cores>

All-by-all blast. Copy all the taxonID.fa.cdhit files (or .cdhitest files) into a new directory. Note that it is important to set the maximum number of hits very high (e.g. 1000) to allow the inclusion of all closely related ingroup and outgroup sequences. I usually use an evalue cutoff of 10 so that I don't need to re-run the all-by-all blast again. You can always setup a more stringent evalue cutoff later. 

Since blastp takes much longer to complete than blastn, I prefer using seperate input fasta files for all-by-all blastp to keep track of progress. I also carry out the makeblastdb step locally before doing blast on a cluster. This is because makeblastdb will check formatting of sequences, duplicate sequence names and special characters in seqIDs etc. It is easier to fix these locally before moving to a cluster.
	
	python all-by-all_blastp.py <DIR> <file_ending> <num_cores>
	cat *.rawblastp >all.rawblastp

Or alternatively, if using cds:
	
	cat *.cdhitest >all.fa
	makeblastdb -in all.fa -parse_seqids -dbtype nucl -out all.fa
	blastn -db all.fa -query all.fa -evalue 10 -num_threads <num_cores> -max_target_seqs 1000 -out all.rawblastn -outfmt '6 qseqid qlen sseqid slen frames pident nident length mismatch gapopen qstart qend sstart send evalue bitscore'

[Optional] Remove ends of sequences that are not covered by any blast hits from other taxa. Skip this if with not to cut ends that are fast-evolving, or using sequences from genome annotation.
	
	cat *.rawblast* >all.rawblast
	python cut_seq_ends.py all.fa all.rawblast

Filter raw blast output by hit fraction and prepare input file for mcl. The input can be rawblastp or rawblastn results. I usually use 0.3 or 0.4 for hit_fraction when using sequences assembled from RNA-seq depending on how divergent the sequences are. A low hit-fraction cutoff will output clusters with more incomplete sequences and much larger and sparser alignments, whereas a high hit-fraction cutoff gives tighter clusters but ignores incomplete or divergent sequences. For genome data I use a hit_fraction cutoff of 0.5. You can also set IGNORE_INTRASPECIFIC_HITS to be true to avoid recent gene duplications or isoforms forming tight clusters and break off. 

	python all-by-all_blast_to_mcl.py all.rawblast <hit_fraction_cutoff>

The output file is used as input for mcl. Try a few different hit fraction cutoffs and inflation values. My experience is that MCL is robust to minusLogEvalue cutoffs and using a cutoff of 0 should works ok. You loose entire clusters of short genes by using a high minusLogEvalue cutoff. Use the smallest inflation value and hit-fraction cutoff value combination that gives alignable clusters. "--te" specifies number of threads, "-I" specifies the inflation value, and -tf 'gq()' specifies minimal -log transformed evalue to consider, and "-abc" specifies the input file format. Here are some example mcl command lines:

	mcl all.rawblast.hit-frac0.4.minusLogEvalue --abc -te 5 -tf 'gq(5)' -I 1.4 -o hit-frac0.4_I1.4_e5
	mcl all.rawblast.hit-frac0.4.minusLogEvalue --abc -te 5 -tf 'gq(5)' -I 2 -o hit-frac0.4_I2_e5
	mcl all.rawblast.hit-frac0.3.minusLogEvalue --abc -te 5 -tf 'gq(5)' -I 1.4 -o hit-frac0.3_I1.4_e5
	mcl all.rawblast.hit-frac0.3.minusLogEvalue --abc -te 5 -tf 'gq(5)' -I 2 -o hit-frac0.3_I2_e5

Write fasta files for each cluster from mcl output. Make a new directory to put the thousands of output fasta files.
	
	mkdir <outDIR>
	python write_fasta_files_from_mcl.py <fasta> <mcl_outfile> <minimal_taxa> <outDIR>

Now we have a new directory with fasta files that look like cluster1.fa, cluster2.fa and so on.

	
##Step 5: Build homolog trees

Note: To put phyutility in your path. First, get the absolute path for the phyutility.jar file. For example, mine is /home/yayang/apps/phyutility/phyutility.jar. Next, make a new text file named "phyutility" in your path, and write this line in it pointing to the absolute path of the jar file like this (use the actual path in your machine of course):

	java -Xmx6g -jar /home/yayang/apps/phyutility/phyutility.jar $*

Make sure that raxml, fasttree, phyutility, mafft and pasta are properly installed and excutables are in the path.

cd into the working directory with unaligned fasta files. Align each cluster, trim alignment, and infer a tree. For clusters that have less than 1000 sequences, it will be aligned with mafft (--genafpair --maxiterate 1000), trimmed by a minimal column occupancy of 0.1 and tree inference using raxml. For larger clusters it will be aligned with pasta, trimmed by a minimal column occupancy of 0.01 and tree inference using fasttree. The ouput tree files look like clusterID.raxml or clusterID.fasttree for clusters with 1000 or more sequences. 

	python fasta_to_tree.py <number_cores> DNA/aa

Trim tips that are longer than a relative length cutoff and more than 10 times longer than its sister. Also trim tips that are longer than an absolute value. The output tree ends with ".tt". Keep input and output trees in the same directory.

	python trim_tips.py <DIR> <relative_cutoff> <absolute_cutoff>

Mask both mono- and paraphyletic tips that belong to the same taxon. Keep the tip that has the most un-ambiguous charactors in the trimmed alignment. Keep input and output trees in the same directory.

	python mask_tips_by_taxonID_transcripts.py <.ttDIR> <aln-clnDIR>

For phylogenomic data sets that are mostly annotated genomes, I would only mask monophyletic tips, and keep the sequence with the shortest terminal branch length. Keep input and output trees in the same directory.

	python mask_tips_by_taxonID_genomes.py <.ttDIR>

Cut deep paralogs. If interested in building phylogeny a lower (more stringent) long_internal_branch_cutoff should be used. Use a higher (more relaxed) cutoff if interested in homologs to avoid splitting homologs. This works very well with CDS and less effective amino acid squences. For CDS the branch lengths are mostly determined by synonymous distance and are more consistant than for amino acids. Make sure that the indir and outdir are different directories.

	python cut_long_internal_branches.py <inDIR> <file_ending> <internal_branch_length_cutoff> <minimal no. taxa> <outDIR>

Alternatively one can calculate the synonymous distance and use that to guide cutting. However, since we are only trying to get well-aligned clusters for tree inference, choice of length cutoffs here can be somewhat arbitary. 

Write fasta file from trees and repeat the steps of aligning, trimming, tree estimating and tree trimming. Can use a set of more stringent cutoffs in the second round. A third round can be done for a subset of clusters.

	python <write_fasta_files_from_trees.py> <fasta> <treDIR> <tree_file_ending> <outDIR>

From here a number of further analyses can be done with the homologs, such as gene tree discordance and back translate peptide alignment to codons with pal2nal and investigate signature of natural selection.


##Step 6: Paralogy pruning to infer orthologs. Use one of the following:

1to1: only look at homologs that are strictly one-to-one. No cutting is carried out.
	
	python filter_one-to-one_orthologs.py <homologDIR> <minimal_taxa> <outDIR>

MI: prune by maximum inclusion. The long_tip_cutoff here is typically the same as the value used when trimming tips. Set OUTPUT_1to1_ORTHOLOGS to False if wish only to ouput orthologs that is not 1-to-1, for example, when 1-to-1 orthologs have already been analyzed in previous steps.

	python prune_paralogs_MI.py <homologDIR> <long_tip_cutoff> <minimal_taxa> <outDIR>

MO: prune by using homologs with monophyletic, non-repeating outgroups, reroot and cut paralog from root to tip. If no outgroup, only use those that do not have duplicated taxa. Change the list of ingroup and outgroup names first. Set OUTPUT_1to1_ORTHOLOGS to False if wish only to ouput orthologs that is not 1-to-1

	python prune_paralogs_MO.py <homologDIR> <minimal_taxa> <outDIR>

RT: prune by extracting ingroup clades and then cut paralogs from root to tip. If no outgroup, only use those that do not have duplicated taxa. Compile a list of ingroup and outgroup taxonID, with each line begin with either "IN" or "OUT", followed by a tab, and then the taxonID.

	python prune_paralogs_RT.py <homologDIR> <outDIR> <minimal_ingroup_taxa> <list of ingroup and outgroup taxonIDs>

Or alternatively, if the input homolog tree is already rooted:

	python prune_paralogs_from_rooted_trees.py <homoTreeDIR> <minimal_taxa> <outDIR>


##Step 7: Visualize matrix occupancy stats and constructing the supermatrix

	python ortholog_occupancy_stats.py <ortho_treDIR>

Reaqd in and rank number of taxa per ortholog from highest to lowest. Plot the ranked number of taxa per ortholog

	a = read.table('ortho_stats')
	downo = order(a[,1],decreasing=T)
	pdf(file="taxon_occupancy.pdf")
	plot(a[downo,1])
	dev.off()

Check taxon_stats to see if any taxa have unusally low number of genes in the orthologs. Open the file taxon_occupancy.pdf and decide the MIN_TAXA filter. Write new fasta files from ortholog trees

	python write_ortholog_fasta_files.py <fasta file with all seqs> <ortholog tree DIR> outDIR MIN_TAXA

Align final orthologs. Play with a few alignment methods here. Try prank or fsa in addition to mafft or sate for more accurate alignments. Prank tend to create lots of gaps when it is not sure about the homology so make sure to check the alignment visually.

	python prank_wrapper.py <inDIR> <outDIR> <file ending> DNA/aa

Trim alignment. I usually use 0.3 for MIN_COLUMN_OCCUPANCY

	python phyutility_wrapper.py <inDIR> <MIN_COLUMN_OCCUPANCY> DNA/aa

Or use Gblocks for trimming alignments if the sequences are very divergent (change FILE_ENDING first):

	python pep_gblocks_wrapper.py <inDIR> <outDIR>

Choose the minimal cleaned alignment length and minimal number of taxa filters for whether to include an ortholog in the supermatrix. Concatenate selected cleaned matrices:

	python concatenate_matrices.py <aln-clnDIR> <numofsites> <numoftaxa> DNA/aa <outfile>

This will output a list of cleaned orthology alignments that passed the filter, a summary of taxon matrix occupancies to check whether any taxon is under represented, and a concatenated matrix in phylip format


##Step 8: Estimate species tree

Run raxml with each ortholog as a separate partition. Use GTRCAT instead of PROTCATWAG for dna

	raxmlHPC-PTHREADS-SSE3 -T <num_cores> -p 12345 -m PROTCATWAG -q <.model> -s <.phy> -n <output>

Run raxml with 200 rapid bootstrap replicates and search for the best tree. Use GTRCAT instead of PROTCATWAG for dna

	raxmlHPC-PTHREADS-SSE3 -T 9 -f a -x 12345 -# 200 -p 12345 -m PROTCATWAG -q <.models file> -s <.phy file> -n <output>

Use examl if the matrix is large:
	
	python examl_wrapper.py <.phy file> <.model file> <outname> <number_cores> DNA/aa

Run 200 jackknife replicates. Copy both the .phy and .model file into a new working dir, cd into the working dir, and run the following script (sample fixed proportion of number of genes):

	python jackknife_by_percent_genes.py <num_core> <jackknife proportion> DNA/aa

Alternatively, resampling by number of genes:

	python jackknife_by_number_genes.py <num_core> <no. genes to resample> DNA/aa

Mapping jackknife results to the best tree. Use GTRCAT instead of PROTCATWAG for dna

	cat *result* >JK10_trees
	raxmlHPC-PTHREADS-SSE3 -f b -t <bestTree> -z JK10_trees -T 2 -m PROTCATWAG -n <output_name>

Making a consensus tree from jackknife output

	phyutility -con -t 0.001 -in JK10_trees -out JK10_consensus

Translate taxon codes to make the tree more readable:
	
	python taxon_name_subst.py <table> <treefile>
	
