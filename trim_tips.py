"""
Trim tips that sticking out (> relative_cutoff and >10 times longer than sister)
Also trim any tips that are > absolute_cutoff
"""

import newick3
import phylo3
import os
import sys
from Bio import SeqIO

# if taxon id pattern changes, change it here


def get_name(name):
    return name.split("@")[0]

# return the outlier tip, with abnormal high contrast and long branch


def check_countrast_outlier(node0, node1, above0, above1):
    if node0.istip and above0 > relative_cutoff:
        if above1 == 0.0 or above0 / above1 > 10:
            return node0
    if node1.istip and above1 > relative_cutoff:
        if above0 == 0.0 or above1 / above0 > 10:
            return node1
    return None


def remove_a_tip(root, tip_node):
    print tip_node.label, tip_node.length
    node = tip_node.prune()
    if len(root.leaves()) > 3:
        node, root = remove_kink(node, root)
        return root
    else:
        print "Less than four tips left"
        return None


def cut_long_tips(curroot):
    going = True
    while going and len(curroot.leaves()) > 3:
        going = False
        for i in curroot.iternodes(order=1):  # POSTORDER
            if i.nchildren == 0:  # at the tip
                i.data['len'] = i.length
                if i.length > absolute_cutoff:
                    curroot = remove_a_tip(curroot, i)
                    going = True
                    break
            elif i.nchildren == 1:
                sys.exit("kink in tree")
            elif i.nchildren == 2:  # bifurcating internal nodes
                child0, child1 = i.children[0], i.children[1]
                above0, above1 = child0.data['len'], child1.data['len']
                # stepwise average
                i.data['len'] = ((above0 + above1) / 2.) + i.length
                outlier = check_countrast_outlier(
                    child0, child1, above0, above1)
                if outlier != None:
                    curroot = remove_a_tip(curroot, outlier)
                    going = True  # need to keep checking
                    break
            else:  # 3 or more branches from this node. Pair-wise comparison
                total_len = 0
                nchild = i.nchildren
                for child in i.children:
                    total_len += child.data['len']
                i.data['len'] = total_len / float(i.nchildren)
                keep_checking = True
                for index1 in range(nchild):  # do all the pairwise comparison
                    for index2 in range(nchild):
                        if index2 <= index1:
                            continue  # avoid repeatedly checking a pair
                        child1, child2 = i.children[index1], i.children[index2]
                        above1, above2 = child1.data['len'], child2.data['len']
                        # print index1,index2, above1, above2
                        outlier = check_countrast_outlier(
                            child1, child2, above1, above2)
                        if outlier != None:
                            print above1, above2
                            curroot = remove_a_tip(curroot, outlier)
                            going = True  # need to keep checking
                            keep_checking = False  # to break the nested loop
                            break
                    if not keep_checking:
                        break
    return curroot

# smooth the kink created by prunning
# to prevent creating orphaned tips after prunning twice at the same node


def remove_kink(node, curroot):
    if node == curroot and curroot.nchildren == 2:
        # move the root away to an adjacent none-tip internal node
        if curroot.children[0].istip:  # the other child is not tip
            curroot = phylo3.reroot(curroot, curroot.children[1])
        else:  # tree has >=4 leaves so the other node cannot be tip
            curroot = phylo3.reroot(curroot, curroot.children[0])
    #---node---< all nodes should have one child only now
    length = node.length + (node.children[0]).length
    par = node.parent
    kink = node
    node = node.children[0]
    # parent--kink---node<
    par.remove_child(kink)
    par.add_child(node)
    node.length = length
    return node, curroot


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "python trim_tips.py DIR relative_cutoff absolute_cutoff"
        sys.exit(0)

    DIR = sys.argv[1] + "/"
    relative_cutoff = float(sys.argv[2])
    absolute_cutoff = float(sys.argv[3])

    filecount = 0
    for i in os.listdir(DIR):
        # only look at raw raxml or fasttree output
        if i[-6:] == ".raxml" or i[-9:] == ".fasttree":
            print i
            filecount += 1
            with open(DIR + i, "r") as infile:
                intree = newick3.parse(infile.readline())
            with open(DIR + i + ".tt", "w") as outfile:
                outfile.write(newick3.tostring(cut_long_tips(intree)) + ";\n")

    if filecount == 0:
        print "No file name with '.raxml' or '.fasttree'found in the treDIR"
