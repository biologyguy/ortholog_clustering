"""
copy both the .phy and .model file into the same working dir
cd into the working dir

Make exclude files for each jackknife replicate and run it 
Also print out a summary file with each line being the gene number that 
was excluded at each jackknife replicate
"""

import os
import sys
import random

REPEATS = 200  # generate 200 jackknife exclude files
# whether or not to use the raxml version compiled with intel compilor
ICC = False

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "usage: python jackknife_by_number_genes.py num_core num_gene_to_resample DNA/aa"
        sys.exit()

    for i in os.listdir("./"):
        if i[-6:] == ".model":
            model_file = i
        elif i[-4:] == ".phy":
            aln = i
    num_core = sys.argv[1]
    resample_num = int(sys.argv[2])
    if sys.argv[3] == "DNA":
        MODEL = "GTRCAT"
    elif sys.argv[3] == "aa":
        MODEL = "PROTCATWAG"
    else:
        print "Input data type: DNA or aa"
        sys.exit()

    print "Found sequence alignment", aln
    print "Found partition file", model_file
    print "Resample", resample_num, "each time for", REPEATS, "replicates"

    # parse the .model file and get the partitions
    infile = open(model_file, "r")
    geneDICT = {}  # key is gene id, value is (start,end)
    for line in infile:
        if len(line) < 3:
            continue
        # line looks like DNA,cluster476-2-1_1.subtree1.p1.aln-cln_gene1=1-1216
        spls = ((line.strip()).split("gene")[1]).split("=")
        geneid = spls[0]
        rg = spls[1].split("-")  # range of position in supermatrx
        geneDICT[geneid] = int(rg[0]), int(rg[1])

    # randomly generate exclude files and run jackknife
    gen_num = len(geneDICT)
    print gen_num, "genes in total"
    jk_num = gen_num - resample_num
    print jk_num, "genes excluded in each jackknife replicate"
    outfile1 = open("jacknife_summary", "w")
    outfile1.write("total number of genes " + str(gen_num) +
                   "\n" + str(jk_num) + "excluded for each replicate\n")
    for i in range(REPEATS):
        exclude = []  # geneIDs to exclude
        while True:
            r = random.randint(1, gen_num)
            if r not in exclude:
                exclude.append(r)
            if len(exclude) == jk_num:
                break
        exclude.sort()
        exc_file = str(resample_num) + "genes_rep" + str(i + 1)
        with open(exc_file, "w") as outfile:
            for j in exclude:
                outfile.write(
                    str(geneDICT[str(j)][0]) + "-" + str(geneDICT[str(j)][1]) + " ")
                outfile1.write(str(j) + " ")
        outfile1.write("\n")
        # run raxml for this jackknife replicate
        if ICC:
            os.system("module purge")
            os.system("module load openmpi/1.2.6-intel")
            raxml_call = "raxmlHPC-PTHREADS-SSE3-icc"
        else:
            raxml_call = "raxmlHPC-PTHREADS-SSE3"
        cmd = raxml_call + " -F -T " + num_core + " -E " + \
            exc_file + " -m " + MODEL + " -p 6666 -q " + model_file
        cmd += " -s " + aln + " -n " + aln.replace("phy", exc_file)
        print cmd
        os.system(cmd)  # write new matrix
        cmd = raxml_call + " -F -T " + num_core + " -m " + \
            MODEL + " -p 6666 -q " + model_file + "." + exc_file
        cmd += " -s " + aln + "." + exc_file + \
            " -n " + aln.replace("phy", exc_file)
        print cmd
        os.system(cmd)  # run raxml on the subsampled alignment
    outfile1.close()
