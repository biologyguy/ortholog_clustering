"""
If no outgroup, only output ingroup clades with no taxon repeats
If outgroup present, extract rooted ingroup clades and prune paralogs

Prepare a taxon file, with each line look like (separated by tab):
IN	taxonID1
IN	taxonID2
OUT	taxonID3
"""

import phylo3
import newick3
import os
import sys

HOMOTREE_ENDING = ".tre"

# if pattern changes, change it here
# given tip label, return taxon name identifier


def get_name(label):
    return label.split("@")[0]


def get_front_labels(node):
    leaves = node.leaves()
    return [i.label for i in leaves]


def get_back_labels(node, root):
    all_labels = get_front_labels(root)
    front_labels = get_front_labels(node)
    return set(all_labels) - set(front_labels)  # labels do not repeat


def get_front_names(node):  # may include duplicates
    labels = get_front_labels(node)
    return [get_name(i) for i in labels]


def get_back_names(node, root):  # may include duplicates
    back_labels = get_back_labels(node, root)
    return [get_name(i) for i in back_labels]


def remove_kink(node, curroot):
    if node == curroot and curroot.nchildren == 2:
        # move the root away to an adjacent none-tip
        if curroot.children[0].istip:  # the other child is not tip
            curroot = phylo3.reroot(curroot, curroot.children[1])
        else:
            curroot = phylo3.reroot(curroot, curroot.children[0])
    #---node---< all nodes should have one child only now
    length = node.length + (node.children[0]).length
    par = node.parent
    kink = node
    node = node.children[0]
    # parent--kink---node<
    par.remove_child(kink)
    par.add_child(node)
    node.length = length
    return node, curroot

# input is a tree with both ingroups and more than 1 outgroups


def extract_ingroup_clades(root):
    inclades = []
    while True:
        max_score, direction, max_node = 0, "", None
        for node in root.iternodes():
            front, back = 0, 0
            front_names_set = set(get_front_names(node))
            for name in front_names_set:
                if name in OUTGROUPS:
                    front = -1
                    break
                else:
                    front += 1
            back_names_set = set(get_back_names(node, root))
            for name in back_names_set:
                if name in OUTGROUPS:
                    back = -1
                    break
                else:
                    back += 1
            if front > max_score:
                max_score, direction, max_node = front, "front", node
            if back > max_score:
                max_score, direction, max_node = back, "back", node
        # print max_score,direction
        if max_score >= MIN_INGROUP_TAXA:
            if direction == "front":
                inclades.append(max_node)
                kink = max_node.prune()
                if len(root.leaves()) > 3:
                    newnode, root = remove_kink(kink, root)
                else:
                    break
            elif direction == "back":
                par = max_node.parent
                par.remove_child(max_node)
                max_node.prune()
                inclades.append(phylo3.reroot(root, par))  # flip dirction
                if len(max_node.leaves()) > 3:
                    max_node, root = remove_kink(max_node, max_node)
                else:
                    break
        else:
            break
    return inclades


def get_ortho_from_rooted_inclade(inclade):
    orthologs = []  # store ortho clades
    clades = [inclade]
    while True:
        newclades = []  # keep track of subclades generated in this round
        for clade in clades:
            num_taxa = len(set(get_front_names(clade)))
            num_tips = len((get_front_labels(clade)))
            if num_taxa == num_tips:  # no taxon repeats
                orthologs.append(clade)
            else:  # has duplicated taxa
                for node in clade.iternodes(order=0):  # PREORDER, root to tip
                    if node.istip:
                        continue
                    # traverse the tree from root to tip
                    child0, child1 = node.children[0], node.children[1]
                    name_set0 = set(get_front_names(child0))
                    name_set1 = set(get_front_names(child1))
                    if len(name_set0.intersection(name_set1)) > 0:
                        if node == clade:
                            # break by bifid at the base
                            newclades += [child0, child1]
                        # cut the side with less taxa
                        elif len(name_set0) > len(name_set1):
                            node.remove_child(child1)
                            child1.prune()
                            # no rerooting here
                            node, clade = remove_kink(node, clade)
                            newclades += [clade, child1]
                        else:
                            node.remove_child(child0)
                            child0.prune()
                            # no rerooting here
                            node, clade = remove_kink(node, clade)
                            newclades += [clade, child0]
                        break
        if newclades == []:
            break
        clades = newclades
    return orthologs


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print "python prune_paralogs_RT.py homoTreeDIR outDIR MIN_INGROUP_TAXA TAXON_CODE"
        sys.exit(0)

    homoDIR = sys.argv[1] + "/"
    outDIR = sys.argv[2] + "/"
    MIN_INGROUP_TAXA = int(sys.argv[3])
    taxon_code_file = sys.argv[4]

    INGROUPS = []
    OUTGROUPS = []
    with open(taxon_code_file, "r") as infile:
        for line in infile:
            if len(line) < 3:
                continue
            spls = line.strip().split("\t")
            if spls[0] == "IN":
                INGROUPS.append(spls[1])
            elif spls[0] == "OUT":
                OUTGROUPS.append(spls[1])
            else:
                print "Check TAXON_CODE file format"
                sys.exit()
    if len(set(INGROUPS) & set(OUTGROUPS)) > 0:
        print "Taxon ID", set(INGROUPS) & set(OUTGROUPS), "in both ingroups and outgroups"
        sys.exit(0)
    print len(INGROUPS), "ingroup taxa and", len(OUTGROUPS), "outgroup taxa read"
    print "Ingroups:", INGROUPS
    print "Outgroups:", OUTGROUPS

    for treefile in os.listdir(homoDIR):
        if treefile[-len(HOMOTREE_ENDING):] != HOMOTREE_ENDING:
            continue
        with open(homoDIR + treefile, "r") as infile:
            intree = newick3.parse(infile.readline())
        curroot = intree
        all_names = get_front_names(curroot)
        num_tips = len(all_names)
        num_taxa = len(set(all_names))
        print treefile

        # check taxonIDs
        ingroup_names = []
        outgroup_names = []
        for name in all_names:
            if name in INGROUPS:
                ingroup_names.append(name)
            elif name in OUTGROUPS:
                outgroup_names.append(name)
            else:
                print name, "not in ingroups or outgroups"
                sys.exit()
        if len(set(ingroup_names)) < MIN_INGROUP_TAXA:
            print "not enough ingroup taxa in tree"
            continue

        # at least one outgroup present, root and cut inclades
        if len(outgroup_names) > 0:
            inclades = extract_ingroup_clades(curroot)
            inclade_count = 0
            for inclade in inclades:
                inclade_count += 1
                inclade_name = outDIR + \
                    treefile.replace(
                        HOMOTREE_ENDING, ".inclade") + str(inclade_count)
                with open(inclade_name, "w") as outfile:
                    outfile.write(newick3.tostring(inclade) + ";\n")
                orthologs = get_ortho_from_rooted_inclade(inclade)
                ortho_count = 0
                for ortho in orthologs:
                    if len(get_front_labels(ortho)) >= MIN_INGROUP_TAXA:
                        ortho_count += 1
                        with open(inclade_name + ".ortho" + str(ortho_count) + ".tre", "w") as outfile:
                            outfile.write(newick3.tostring(ortho) + ";\n")

        elif len(all_names) == num_taxa:
            # only output ortho tree when there is no taxon repeats
            with open(outDIR + treefile.replace(HOMOTREE_ENDING, ".unrooted-ortho.tre"), "w") as outfile:
                outfile.write(newick3.tostring(curroot) + ";\n")

        # do not attempt to infer direction of gene duplication without
        # outgroup info
        else:
            print "duplicated taxa in unrooted tree"
