"""
refine alignments using SATe. Modify SATEDIR according to the location of
the Sate directory 

input:
- alignments that end with .aln

output:
- refined alignments
"""
import sys
import os
import newick3
from Bio import SeqIO

SATEDIR = "~/apps/satesrc-v2.2.7-2013Feb15"
MAX_ITER = 3
MIN_SATE_SEQ = 200  # do not refine when alignment has less than this number

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print "usage: python pep_sate_wrapper.py inDIR outDIR NUM_CORES DNA/aa"
        sys.exit()

    inDIR = sys.argv[1] + "/"
    outDIR = sys.argv[2] + "/"
    NUM_CORES = sys.argv[3]
    if sys.argv[4] == "aa":
        seqtype = "Protein"
    elif sys.argv[4] == "DNA":
        seqtype = "DNA"
    else:
        print "Input data type: DNA or aa"
        sys.exit()

    done = os.listdir(outDIR)
    filecount = 0
    for i in os.listdir(inDIR):
        if i[-4:] != ".aln" or "sate" in i:
            continue
        clusterID = i.split(".")[0]
        if clusterID + ".sate.aln" in done:
            continue
        print clusterID
        filecount += 1

        seqcount = 0  # record how many sequences in the alignment
        with open(inDIR + i, "r") as infile:
            for line in infile:
                if line[0] == ">":
                    seqcount += 1
        if seqcount < MIN_SATE_SEQ:
            os.system(
                "cp " + inDIR + i + " " + outDIR + clusterID + ".sate.aln")
            continue  # no refinement

        # sate does not recognize "*" or "U"
        handle = open(inDIR + i, "r")
        outfile = open(inDIR + i + ".temp", "w")
        for record in SeqIO.parse(handle, "fasta"):
            seqid, seq = str(record.id), str(record.seq)
            # remove U which is usually not in aa alphabet
            seq = (seq.replace("U", "X")).replace("u", "x")
            # remove stop codon and seq after it since sate crashes at stop
            # codon
            seq = seq[:seq.find("*")]
            outfile.write(">" + seqid + "\n" + seq + "\n")
        handle.close()
        outfile.close()

        # call sate
        cmd = "python " + SATEDIR + \
            "/sate-core/run_sate.py --input=" + inDIR + i + ".temp"
        cmd += " --aligned --datatype=" + seqtype + " --num-cpus=" + NUM_CORES
        cmd += " --iter-limit=" + str(MAX_ITER)
        print cmd
        os.system(cmd)
        os.system("mv " + inDIR + "satejob.marker001." + i +
                  ".temp.aln " + outDIR + clusterID + ".sate.aln")

        # remove intermediate files
        os.system("rm " + inDIR + "satejob*")
        os.system("rm " + inDIR + i + ".temp")

    if filecount == 0:
        print "No file in input directory end with .aln"
