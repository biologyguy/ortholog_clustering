import phylo3
import newick3
import os
import sys

HOMOTREE_ENDING = ".phyutil.tre"
OUTPUT_1to1_ORTHOLOGS = False


def get_name(label):
    return label.split("@")[0]


def get_front_labels(node):
    leaves = node.leaves()
    return [i.label for i in leaves]


def get_front_names(node):  # may include duplicates
    labels = get_front_labels(node)
    return [get_name(i) for i in labels]


def remove_kink(node, curroot):
    if node == curroot and curroot.nchildren == 2:
        # move the root away to an adjacent none-tip
        if curroot.children[0].istip:  # the other child is not tip
            curroot = phylo3.reroot(curroot, curroot.children[1])
        else:
            curroot = phylo3.reroot(curroot, curroot.children[0])
    #---node---< all nodes should have one child only now
    length = node.length + (node.children[0]).length
    par = node.parent
    kink = node
    node = node.children[0]
    # parent--kink---node<
    par.remove_child(kink)
    par.add_child(node)
    node.length = length
    return node, curroot


def get_ortho_from_rooted_inclade(inclade):
    orthologs = []  # store ortho clades
    clades = [inclade]
    while True:
        newclades = []  # keep track of subclades generated in this round
        for clade in clades:
            num_taxa = len(set(get_front_names(clade)))
            num_tips = len((get_front_labels(clade)))
            if num_taxa == num_tips:  # no taxon repeats
                orthologs.append(clade)
            else:  # has duplicated taxa
                for node in clade.iternodes(order=0):  # PREORDER, root to tip
                    if node.istip:
                        continue
                    # traverse the tree from root to tip
                    child0, child1 = node.children[0], node.children[1]
                    name_set0 = set(get_front_names(child0))
                    name_set1 = set(get_front_names(child1))
                    if len(name_set0.intersection(name_set1)) > 0:
                        if node == clade:
                            # break by bifid at the base
                            newclades += [child0, child1]
                        # cut the side with less taxa
                        elif len(name_set0) > len(name_set1):
                            node.remove_child(child1)
                            child1.prune()
                            # no rerooting here
                            node, clade = remove_kink(node, clade)
                            newclades += [clade, child1]
                        else:
                            node.remove_child(child0)
                            child0.prune()
                            # no rerooting here
                            node, clade = remove_kink(node, clade)
                            newclades += [clade, child0]
                        break
        if newclades == []:
            break
        clades = newclades
    return orthologs

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "python python prune_paralogs_from_rooted_trees.py homoTreeDIR minimal_taxa outDIR"
        sys.exit(0)

    inDIR = sys.argv[1] + "/"
    MIN_TAXA = int(sys.argv[2])
    outDIR = sys.argv[3] + "/"
    for i in os.listdir(inDIR):
        if i[-len(HOMOTREE_ENDING):] != HOMOTREE_ENDING:
            continue
        print i
        outID = outDIR + i.replace(HOMOTREE_ENDING, "")
        with open(inDIR + i, "r") as infile:
            intree = newick3.parse(infile.readline())
        curroot = intree
        orthologs = get_ortho_from_rooted_inclade(curroot)
        count = 1
        for ortho in orthologs:
            if len(set(get_front_names(ortho))) >= MIN_TAXA:
                with open(outID + ".ortho" + str(count) + ".tre", "w") as outfile:
                    outstring = newick3.tostring(ortho)
                    #outstring = outstring.replace(":0",":1")
                    outfile.write(outstring + ";\n")
                count += 1
