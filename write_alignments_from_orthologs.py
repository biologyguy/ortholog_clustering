"""
Read alignment after cutting
write individual alignment files for each ortholog
Use this instead of "write_fasta_from_orthologs.py" when the final alignment
was refined using sate

Since no taxon repeats
Shorten seq id to taxon id
"""

import sys
import os
import newick3
import phylo3
from Bio import SeqIO

ORTHO_TREE_FILE_ENDING = ".tre"


def get_name(label):
    return label.split("@")[0]


def get_front_labels(node):
    leaves = node.leaves()
    return [i.label for i in leaves]

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "usage: python write_alignments_from_orthologs.py alnDIR orthoTreeDIR outDIR"
        sys.exit()

    alnDIR = sys.argv[1] + "/"
    treDIR = sys.argv[2] + "/"
    outDIR = sys.argv[3] + "/"

    for i in os.listdir(treDIR):
        if i[-len(ORTHO_TREE_FILE_ENDING):] == ORTHO_TREE_FILE_ENDING:
            clusterID = (i.split(".")[0]).split("_")[0]
            spls = clusterID.split("-")
            # when using pep
            alnname = spls[0] + "-" + spls[1] + ".sate.aln"
            # when using cds
#alnname = spls[0]+"-"+spls[1]+"-"+spls[2]+".cds.fa.aln"
            print i, alnname

            # read in the alignment into an dictionary
            seqDICT = {}  # key is seqID, value is seq
            with open(alnDIR + alnname, "rU") as handle:
                for record in SeqIO.parse(handle, "fasta"):
                    seqDICT[str(record.id)] = str(record.seq)

            # read in tree tips and write output alignment
            with open(treDIR + i, "r")as infile:
                intree = newick3.parse(infile.readline())
            labels = get_front_labels(intree)
            with open(outDIR + i.replace(ORTHO_TREE_FILE_ENDING, ".aln"), "w") as outfile:
                for lab in labels:
                    outfile.write(
                        ">" + get_name(lab) + "\n" + seqDICT[lab] + "\n")
