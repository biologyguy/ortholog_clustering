#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Created on: Feb 26 2015 

"""
Identify the best values for inflation and evalue cutoff for clustering orthologs with MCL
"""


def parse_clusters(file_handle):
    """
    Create a dictionary with indices for every sequence, containing all other sequences in their group
    """
    clusters = {}
    for group in file_handle:
        genes = group.split("\t")
        for gene in genes:
            clusters[gene.strip()] = [x.strip() for x in genes]
    return clusters


def score_mcl_run(_known_orthos, _mcl_clusters, quiet=False):
    reward = 1
    penalty = -1
    score = 0

    # check that orthologs are in the same group
    for ortho in _known_orthos:
        if ortho not in _mcl_clusters:
            if not quiet:
                print("%s not found in MCL clusters file" % ortho)
            continue
        for ortho_check in _known_orthos[ortho]:
            if ortho_check in _mcl_clusters[ortho]:
                score += reward
            else:
                score += penalty

    # check that paralogs are not in the same group
    for cluster in _mcl_clusters:
        if cluster in _known_orthos:
            # remove all matched orthologs from curated set first, and then remove extra sequences not in curated set
            non_shared = [x for x in _mcl_clusters[cluster] if x not in _known_orthos[cluster]]
            paralogs = [x for x in non_shared if x in _known_orthos]
            score -= len(paralogs)

    return score

if __name__ == '__main__':
    from os.path import abspath
    from mcmcmc import *
    from subprocess import Popen
    from MyFuncs import TempDir
    from multiprocessing import cpu_count

    import argparse
    parser = argparse.ArgumentParser(prog="inflation_pred", formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                     description="mcmc to determine the optimal inflation value for the MCL program")

    parser.add_argument("orthologs", help="File containing known ortholog groups", action="store")
    parser.add_argument("all_by_all_blast", help="File containing tab formatted blast output", action="store")

    parser.add_argument("-I", "--inflation_range", nargs=2,
                        help="Specify the min/max values of the MCL inflation parameter")
    parser.add_argument("-gq", "--gq_range", nargs=2,
                        help="Specify the min/max values for blast score threshold")
    parser.add_argument("-s", "--steps", help="Specify how many mcmcmc steps to be run (default=1000)", type=int)
    in_args = parser.parse_args()

    def mcl_params(_vars):
        temp_dir = TempDir()
        I, gq = _vars
        Popen("mcl %s --abc -tf 'gq(%s)' -te 3 -I %s -o %s/mcl_output 2> /dev/null" %
              (all_by_all, gq, I, temp_dir.path), shell=True).wait()

        with open("%s/mcl_output" % temp_dir.path, "r") as ifile:
            mcl_clusters = parse_clusters(ifile)

        with open(orthologs, "r") as ifile:
            known_orthos = parse_clusters(ifile)

        Popen("rm -r %s/*" % temp_dir.path, shell=True).wait()

        mcl_run = score_mcl_run(known_orthos, mcl_clusters, quiet=True)
        return mcl_run

    orthologs = abspath(in_args.orthologs)
    all_by_all = abspath(in_args.all_by_all_blast)
    inflation_range = [1.01, 150.] if not in_args.inflation_range else [float(x) for x in in_args.inflation_range]
    blast_range = [20., 400.] if not in_args.gq_range else [float(x) for x in in_args.gq_range]
    steps = 1000 if not in_args.steps else in_args.steps

    mcl_variables = [Variable("I", inflation_range[0], inflation_range[1]), Variable("gq", blast_range[0], blast_range[1])]

    mcmcmc = MCMCMC(mcl_variables, mcl_params, steps=steps, sample_rate=1)
    mcmcmc.run()
    print("Best: %s" % mcmcmc.best)

    """
    # Best possible score: 1447
    """
