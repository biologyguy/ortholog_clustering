#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
fix EST names to taxonID@seqID before transdecoder
"""

import os
import argparse

# seq names look like
#>cds.Mecr@gi_11549800_gb_BF478973_1_BF478973|m.9518 Mecr@gi_11549800_gb_BF478973_1_BF478973|g.9518  ORF Mecr@gi_11549800_gb_BF478973_1_BF478973|g.9518 Mecr@gi_11549800_gb_BF478973_1_BF478973|m.9518 type:3prime_partial len:195 (+) Mecr@gi_11549800_gb_BF478973_1_BF478973:86-667(+)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='fix_names_from_transdecoder', description='Convert the long, awkward names output by TransDecoder to taxonID@seqID')

    parser.add_argument('in_dir', help='Location of transdecoder output', action='store')
    parser.add_argument('out_dir', help="Location for new files", default=os.getcwd(), action='store')

    in_args = parser.parse_args()

    in_dir = os.path.abspath(in_args.in_dir)
    assert os.path.isdir(in_dir)

    out_dir = os.path.abspath(in_args.out_dir)
    assert os.path.isdir(out_dir)

    root, dirs, files = next(os.walk(in_dir))

    taxon_list = []
    for _path in files:
        if _path[-4:] == ".pep" or _path[-4:] == ".cds":
            taxonID = _path.split(".")[0]
            if taxonID not in taxon_list:
                print("### %s ###" % taxonID)
                taxon_list.append(taxonID)

            infile = open("%s/%s" % (in_dir, _path), "rU")
            outname = taxonID
            if _path[-4:] == ".pep":
                outname += ".fa"
            else:
                outname += ".fa.cds"

            outfile = open("%s/%s" % (out_dir, outname), "w")
            for line in infile:
                if line[0] == ">":
                    newid = line.split(" ")[-1]
                    newid = newid.replace("(-)", "_minus")
                    newid = newid.replace("(+)", "_plus")
                    newid = newid.replace("-", "_")
                    newid = newid.replace(":", "_")
                    if "@" not in newid:
                        outfile.write(">%s@%s" % (taxonID, newid))
                    else:
                        outfile.write(">" + newid)
                else:
                    outfile.write(line)
            outfile.close()
            infile.close()
