#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
input: all-by-all blast results into one file

blast output file columns (separated by tab):
0-qseqid 1-sseqid 2-pident 3-length 4-mismatch 5-gapopen 6-qstart
7-qend 8-sstart 9-send 10-evalue 11-bitscore 12-qlen 13-slen 14-nident

Ignore self-hits, filter by hit fraction, and convert evalues to -log(Evalue)
Taxa can be excluded if desired with the -et flag

Set evalue threshold of 0.01
"""

import os
import sys
import math
import argparse


def get_taxon_name(name):
    return name.split("@")[0]


def get_minus_log_evalue(raw_value):
    try:
        result = -math.log10(float(raw_value))
    except:
        result = 180.0  # largest -log10(evalue) seen

    # MCL cannot take negative values
    # Switch to small decimals for bad e-values
    if result < 0:
        result *= -1
    return result

if __name__ == "__main__":
    parser = argparse.ArgumentParser(prog='all_by_all_blast_to_mcl', description='prepare blast results for mcl',
                                     formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('blast_results', help='raw blast file', action='store')
    parser.add_argument('hit_fraction', action='store', type=float,
                        help="Choose a hit fraction cutoff value (i.e., length of match), between 0.0 and 1.0. Higher "
                             "values ignore more matches, but lead to tighter clusters and better alignments. "
                             "Try 0.3-0.4 for RNA-seq assemblies, and 0.5 for genomes.")
    parser.add_argument('-et', '--exclude_taxa', metavar='taxa', nargs="+", action='store',
                        help='If there are taxa that should be ignored, list them here (space delimited)')
    parser.add_argument('-is', '--ignore_intraspecific_hits', action='store_true',
                        help="Ignore hits between sequences within the same taxa.")
    parser.add_argument('-ev', '--evalue', action='store', help="Specify a max evalue (default=0.01)", type=float)
    in_args = parser.parse_args()

    exclude = [] if not in_args.exclude_taxa else in_args.exclude_taxa
    rawblastfile = os.path.abspath(in_args.blast_results)
    hit_frac_cutoff = in_args.hit_fraction
    minus_log_evalue_threshold = 2 if not in_args.evalue else get_minus_log_evalue(in_args.evalue)

    infile = open(rawblastfile, "r")
    best_hits = {}
    for line in infile:
        if len(line) < 3 or line[0] == "#":
            continue  # skip empty lines and comments
        spls = line.strip().split("\t")
        query, hit = spls[0], spls[1]
        if query == hit:
            continue  # skip self hits
        query_taxon = get_taxon_name(query)
        hit_taxon = get_taxon_name(hit)

        if query_taxon in exclude or hit_taxon in exclude:
            continue

        if in_args.ignore_intraspecific_hits and query_taxon == hit_taxon:
            continue

        qlen, qstart, qend = float(spls[12]), float(spls[6]), float(spls[7])
        slen, sstart, send = float(spls[13]), float(spls[8]), float(spls[9])
        minusLogEvalue = get_minus_log_evalue(spls[10])

        if minusLogEvalue < minus_log_evalue_threshold:
            continue

        if (query, hit) in best_hits and best_hits[(query, hit)] < minusLogEvalue:
            best_hits[(query, hit)] = minusLogEvalue
        else:
            perc_qrange = (qend - qstart + 1) / qlen
            perc_srange = (send - sstart + 1) / slen
            if perc_qrange >= float(hit_frac_cutoff) and perc_srange >= float(hit_frac_cutoff):
                best_hits[(query, hit)] = minusLogEvalue

    infile.close()

    outname = "%s.hit-frac%s.minusLogEvalue" % (rawblastfile.split("/")[-1].split(".")[:-1][0], hit_frac_cutoff)

    if len(exclude):
        outname += ".exclude"

    outfile = open(outname, "w")
    count = 0
    for query, hit in best_hits:
        outfile.write("%s %s %s\n" % (query, hit, best_hits[(query, hit)]))
        count += 1

    outfile.close()
    print("%s best hits written to %s" % (count, outname))