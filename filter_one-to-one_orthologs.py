"""
Input: homolog trees
Output: individual ortholog trees without taxon repeats
"""
import newick3
import phylo3
import os
import sys
from Bio import SeqIO

HOMOTREE_ENDING = ".homoTree"


def get_name(name):
    # return name.replace("_R_","")[:4]
    return name.split("@")[0]


def get_front_labels(node):
    leaves = node.leaves()
    return [i.label for i in leaves]


def get_front_names(node):  # may include duplicates
    labels = get_front_labels(node)
    return [get_name(i) for i in labels]

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print "python filter_one-to-one_orthologs.py homoTreeDIR minimal_taxa outDIR"
        sys.exit(0)

    inDIR = sys.argv[1] + "/"
    MIN_TAXA = int(sys.argv[2])
    outDIR = sys.argv[3] + "/"
    infile_count = 0
    outfile_count = 0
    for i in os.listdir(inDIR):
        if i[-len(HOMOTREE_ENDING):] != HOMOTREE_ENDING:
            continue
        infile_count += 1
        with open(inDIR + i, "r") as infile:  # only 1 tree in each file
            intree = newick3.parse(infile.readline())
        curroot = intree
        names = get_front_names(curroot)
        num_tips, num_taxa = len(names), len(set(names))
        print "number of tips:", num_tips, "number of taxa:", num_taxa
        if num_tips == num_taxa and num_taxa >= MIN_TAXA:
            print i, "written to out dir"
            outname = i + ".1to1ortho.tre"
            os.system("cp " + inDIR + i + " " + outDIR + outname)
            outfile_count += 1

    # output summary stats
    if infile_count == 0:
        print "No file with " + HOMOTREE_ENDING + " was found"
    else:
        print infile_count, "files read,", outfile_count, "written to", outDIR
