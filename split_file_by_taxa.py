#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Created on: Feb 23 2015 

"""
DESCRIPTION OF PROGRAM
"""

from Bio import SeqIO
import SeqBuddy
import argparse
import os

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog="split_file_by_taxa", formatter_class=argparse.ArgumentDefaultsHelpFormatter,
                                 description="Separate a file with multiple taxa into individual fasta files. The input "
                                             "sequences must be named with the convention taxon@seq_id.")

    parser.add_argument("in_file", help="Which file would you like separated?", action="store")
    parser.add_argument("-o", '--out_dir', help="Where to send the files?", default=os.getcwd(), action="store")

    in_args = parser.parse_args()

    out_dir = os.path.abspath(in_args.out_dir)
    assert os.path.isdir(out_dir)

    seqbuddy = SeqBuddy.SeqBuddy(in_args.in_file)
    taxon_dic = {}
    failed_ids = []
    for rec in seqbuddy.records:
        seq_id = rec.id.split("@")
        if len(seq_id) == 1:
            failed_ids.append(seq_id)
            continue

        taxa = seq_id[0]
        if taxa in taxon_dic:
            taxon_dic[taxa].append(rec)

        else:
            taxon_dic[taxa] = [rec]

    taxon_dic = {taxa: SeqBuddy.SeqBuddy(taxon_dic[taxa]) for taxa in taxon_dic}

    for taxa in taxon_dic:
        with open("%s/%s.fa" % (out_dir, taxa), 'w') as ofile:
            SeqIO.write(taxon_dic[taxa].records, ofile, "fasta")
